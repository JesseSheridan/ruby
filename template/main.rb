describe 'answer_to_life' do
  it 'should return 42' do
    expect(answer_to_life).to eq(42)
  end
end

def answer_to_life
  42
end
